## ACTIVIDAD 5 - Boostrap5

Esta actividad consiste en la creación de una página web utilizando Bootstrap 5. La página deberá incluir varios componentes como menú, carrusel, citas en bloque, tarjetas, tabla, pestañas, formulario y pie de página.

### Requisitos de la Actividad

Para completar esta actividad, se deben cumplir los siguientes requisitos:

1. Crear una página web utilizando Bootstrap 5.
2. Incluir los siguientes componentes:
   - Menú con opciones de navegación, logo y buscador.
   - Carrusel con imágenes y texto.
   - Blockquotes con tres columnas que contengan título y texto.
   - Cards organizadas en dos filas y 3 cards cada una.
   - Tabla con 5 columnas con efectos hover, striped y un color.
   - Tabs con texto, botón y que sean 3 tabs.
   - Form con 3 campos de entrada con su título.
   - Footer de dos columnas con enlaces y otra columna con iconos de redes sociales, además de información de derechos de autor.
- El sitio web debe ser responsive dependiendo del ancho del navegador.
- [Video de referencia](https://drive.google.com/file/d/1KnZY6pAVoI1utjXzGisS7WUPSaS8cwLn/view)


### Contenido del Repositorio

- **img/**: Carpeta que contiene las imágenes utilizadas y capturas de cómo ha quedado la actividad.
- **Actividad-5-Lenguajes.pdf**: Documento PDF que describe los requisitos de la actividad.
- **index.html**: Archivo principal para la página web de la actividad.



### Estado
[![Estado de construcción](https://img.shields.io/static/v1?label=Estado%20de%20Construcción&message=Finalizado&color=brightgreen)](https://gitlab.com/gusgonza/MP04-actividad-5/-/tree/main)

